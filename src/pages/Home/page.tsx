import React, { useCallback, useEffect, useState } from 'react'
import api from '../../services/api'
import bg from '../../assets/bg.jpg'

import './style.css'
import Card from '../../components/Card';

interface ResponseData {

    id: string;
    name: string;
    description: string;
    thumbnail: {
        path: string;
        extension: string;
    };

}


export const Home = () => {

    const [characters, setCharacters] = useState<ResponseData[]>([])
    const [search, setSearch] = useState('')
    const lowerSearch = search.toLowerCase();

    const filtredChar = characters.filter((char) => char.name.toLowerCase().includes(lowerSearch))


    useEffect(() => {

        const fetch = async () => {

            try {
                const res = await api.get(`characters`)
                setCharacters(res.data.data.results)

            } catch (error) {

            }


        }
        fetch();
    }, [])

    const handleMore = useCallback(async()=>{
        try{
            const offset = characters.length;
            const response = await api.get('characters',{
                params: {
                    offset,
                }
            });

            setCharacters([...characters,...response.data.data.results])
        } catch(err){
            console.log(err)
        }
    },[characters])

    return (
        <>
            <div className='header'>
                <div className='bg'>
                    <img src={bg} alt="" />
                </div>
                <div className='search-bar'>
                    <input
                        type='search'
                        placeholder='Search here'
                        className='search'
                        value={search}
                        onChange={(ev) => setSearch(ev.target.value)}
                    />
                   
                </div>
            </div>
            <div className='content'>
                <ul>
                    {filtredChar.map(char => {

                        return (
                            <li key={char.id}>
                                <Card path={char.thumbnail.path}
                                    extension={char.thumbnail.extension}
                                    name={char.name}
                                    id={char.id}
                                />
                            </li>
                        )
                    })}
                </ul>


            </div>

            <button className='button-more' onClick={handleMore}>More</button>
        </>
    )
}